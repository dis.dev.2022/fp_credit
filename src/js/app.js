"use strict"

import * as usefulFunctions from "./components/functions.js"; // Useful functions
import maskInput from './forms/input-mask.js'; // Inputs mask
import mobileNav from './components/mobile-nav.js';  // Mobile nav
import collapse from './components/collapse.js'; // Collapse block
import SimpleBar from 'simplebar'; // Custom scroll
import tabs from './components/tabs.js'; // Tabs
import Choices from 'choices.js'; // Select plugin
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";
import {WebpMachine} from "webp-hero"

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Вкладки (tabs)
tabs();

// Сворачиваемые блоки
collapse();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Dropdown
Dropdown();

// Custom Select
document.querySelectorAll('[data-select]').forEach(el => {
    const prettySelect = new Choices(el,{
        allowHTML: true,
        searchEnabled: false
    });
});

// Custom Scroll
// Добавляем к блоку атрибут data-simplebar
// Также можно инициализировать следующим кодом, применяя настройки

if (document.querySelectorAll('[data-simplebar]').length) {
    document.querySelectorAll('[data-simplebar]').forEach(scrollBlock => {
        new SimpleBar(scrollBlock, {
            autoHide: false
        });
    });
}

// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});

/* Модуль работы с ползунком */
/*
Документация плагина: https://refreshless.com/nouislider/ */
import "./forms/range.js";

// Sliders
import "./components/sliders.js";

// Form
document.addEventListener('click', (event) => {
    if(event.target.closest('[data-form-privacy]')) {
        const formSend = event.target.closest('[data-form]').querySelector('[data-form-send]')
        console.log(formSend)
        if(event.target.closest('[data-form-privacy]').checked) {
            formSend.disabled = false
        }
        else {
            formSend.disabled = true
        }
    }
})

document.addEventListener('click', (event) => {
    if(event.target.closest('[data-form-send]')) {
        document.querySelector('[data-success-open]').click();
    }
})

// Button
$('.btn')
    .on('mouseenter', function(e) {
        var parentOffset = $(this).offset(),
            relX = e.pageX - parentOffset.left,
            relY = e.pageY - parentOffset.top;
        $(this).find('b').css({top:relY, left:relX});
    })
    .on('mouseout', function(e) {
        var parentOffset = $(this).offset(),
            relX = e.pageX - parentOffset.left,
            relY = e.pageY - parentOffset.top;
        $(this).find('b').css({top:relY, left:relX});
    });
