// Подключение из node_modules
import * as usefulFunctions from "../components/functions.js"; // Полезные функции
import * as noUiSlider from 'nouislider';

export function rangeInit() {

    if (document.querySelector('[data-range]')) {

        const range = document.querySelector('[data-range]')
        const rangeSlider = range.querySelector('[data-range-slider]')
        const rangeField = range.querySelector('[data-range-field]')
        const rangeInput = range.querySelector('[data-range-input]')
        const rangePlaceholder = range.querySelector('[data-range-plaseholder]')
        const rangeMin = parseInt(rangeSlider.dataset.min)
        const rangMax = parseInt(rangeSlider.dataset.max)
        const rangeStart = parseInt(rangeSlider.dataset.start)

        noUiSlider.create(rangeSlider, {
            start: rangeStart,
            step: 10000,
            connect: 'lower',
            orientation: 'horizontal',
            range: {
                'min': rangeMin,
                'max': rangMax,
            }
        });

        rangeSlider.noUiSlider.on('update', function (values, handle) {
            rangeField.value = usefulFunctions.getDigFormat(Math.ceil(values[handle]))
        });


        rangeField.addEventListener('change', function () {
            let sum = this.value.replace(/\s+/g, '');
            rangeSlider.noUiSlider.set(parseInt(sum));
        });

        rangeField.onfocus = function(event) {
            rangeInput.classList.add('range__field--focus')
        };

        rangeField.onblur = function(event) {
            rangeInput.classList.remove('range__field--focus')
        };
    }
}

window.addEventListener("load", function (e) {
    rangeInit();
});


