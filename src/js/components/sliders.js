/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper';

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров
    if (document.querySelector('[data-swiper]')) {
        new Swiper('[data-swiper]', {
            // Подключаем модули слайдера
            // для конкретного случая
            //modules: [Navigation, Pagination],
            /*
            effect: 'fade',
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            */
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            autoHeight: true,
            speed: 800,
            //touchRatio: 0,
            //simulateTouch: false,
            //loop: true,
            //preloadImages: false,
            //lazy: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            // Arrows
            navigation: {
                nextEl: '.swiper__more .swiper__more--next',
                prevEl: '.swiper__more .swiper__more--prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 16,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 32,
                },
            },
            on: {}
        });
    }

    if (document.querySelector('[data-partners]')) {
        new Swiper('[data-partners]', {
            modules: [Navigation],
            autoHeight: true,
            observer: true,
            loop: true,
            observeParents: true,
            slidesPerView: 2,
            spaceBetween: 20,
            navigation: {
                nextEl: '[data-partners-next]',
                prevEl: '[data-partners-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                },
                1250: {
                    slidesPerView: 4,
                    spaceBetween: 30,
                },
            },
        });
    }

    if (document.querySelector('[data-comments]')) {
        new Swiper('[data-comments]', {
            modules: [Navigation],
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 30,
            navigation: {
                nextEl: '[data-comments-next]',
                prevEl: '[data-comments-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                },
                1250: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                },
            },
        });
    }

    if (document.querySelector('[data-cases]')) {
        new Swiper('[data-cases]', {
            modules: [Navigation, Pagination],
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 30,
            pagination: {
                el: '[data-cases-pagination]',
                clickable: true,
            },
            navigation: {
                nextEl: '[data-cases-next]',
                prevEl: '[data-cases-prev]',
            }
        });
    }
}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
